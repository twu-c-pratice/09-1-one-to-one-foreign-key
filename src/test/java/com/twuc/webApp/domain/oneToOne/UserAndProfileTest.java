package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired private UserEntityRepository userEntityRepository;

    @Autowired private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        ClosureValue<Long> userId = new ClosureValue<>();
        ClosureValue<Long> profileId = new ClosureValue<>();
        flushAndClear(
                em -> {
                    UserEntity user = new UserEntity();
                    UserProfileEntity profile = new UserProfileEntity("Li", "Ming");
                    user.setProfile(profile);
                    profile.setUser(user);

                    userEntityRepository.save(user);

                    userId.setValue(user.getId());
                    profileId.setValue(profile.getId());
                });

        run(
                em -> {
                    assertTrue(
                            userProfileEntityRepository.findById(profileId.getValue()).isPresent());
                });

        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        ClosureValue<Long> userId = new ClosureValue<>();
        ClosureValue<Long> profileId = new ClosureValue<>();
        flushAndClear(
                em -> {
                    UserEntity user = new UserEntity();
                    UserProfileEntity profile = new UserProfileEntity("Li", "Ming");
                    user.setProfile(profile);
                    profile.setUser(user);

                    userEntityRepository.save(user);

                    userId.setValue(user.getId());
                    profileId.setValue(profile.getId());
                });
        flushAndClear(
                em -> {
                    UserEntity user = userEntityRepository.findById(userId.getValue()).get();
                    userEntityRepository.delete(user);
                });

        run(
                em -> {
                    assertFalse(
                            userProfileEntityRepository.findById(profileId.getValue()).isPresent());
                });
        // --end->
    }
}
